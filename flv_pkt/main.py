#!/usr/bin/env python
import sys
import struct

def sbe24_to_int32(sbe, upper = b'\0'):
  return struct.unpack('>I', upper + sbe)[0]

def int32_to_sbe24(int32):
  p = struct.pack('>I', int32)
  return (p[1:4], p[0:1])

class pkt:
  has_to_read = 0
  unpack_str = ''

  def __init__(self):
    self.pkt_struct = struct.Struct(self.unpack_str)

  def write(self):
    return b''

  def to_read(self):
    return self.has_to_read

  def handle_header(self, packed_data):
    pass

  def handle_payload(self, packed_data):
    pass

  def parse(self, packed_dat):
    data = self.pkt_struct.unpack(packed_dat)
    self.handle_header(data)

class hdr(pkt):
  unpack_str   = '>3s B B I'
  has_to_read = 9

  signature = ''
  version   = 0
  flags     = 0
  hdr_size  = 0

  def write(self):
    flags = struct.pack('B', self.flags)
    data_size = struct.pack('>I', 9)
    # Header + First PreviousTagSize
    return b'FLV\x01' + flags + data_size + b'\x00\x00\x00\x00'

  def handle_header(self, data):
    self.signature, self.version, self.flags, self.hdr_size = data
    self.has_to_read -= self.hdr_size

  def __str__(self):
    flg = ''
    if self.flags & 0x1:
      flg = 'VIDEO'
    if self.flags & 0x4:
      flg = 'audio' if flg == ''  else flg + '|AUDIO'
    return "Sig: %s, Vers: %d, Flags: %s, Hdr_size: %d" % (
          self.signature, self.version, flg, self.hdr_size)

class flv_previous_tags_info(pkt):
  unpack_str   = '>I'
  has_to_read  = 4

  start_last_pkt = 0

  def handle_header(self, data):
    self.start_last_pkt = data
    self.has_to_read = 0

  def __str__(self):
    return "Start of Last Packet: %d" % self.start_last_pkt

class flv_tags(pkt):
  unpack_str   = '>B 3s 3s 1s 3s'
  has_to_read  = 11

  pkt_type       = 0
  payload_size   = 0
  timestamp      = 0
  stream_id      = 0
  payload        = ''

  def handle_header(self, data):
    (self.pkt_type, payload_size_s, timestamp_lower,
    timestamp_upper, stream_id_s) = data

    self.payload_size = sbe24_to_int32(payload_size_s)
    self.timestamp = sbe24_to_int32(timestamp_lower, timestamp_upper)
    self.stream_id = sbe24_to_int32(stream_id_s)

    self.has_to_read = self.payload_size

  def add_ts(self, time_ms):
    self.timestamp += time_ms

  def handle_payload(self, packed_data):
    self.payload = packed_data
    self.has_to_read -= len(packed_data)

  def write(self):
    pkt_type  = struct.pack('b', self.pkt_type)
    data_size = int32_to_sbe24(self.payload_size)[0]
    timestamp = int32_to_sbe24(self.timestamp)
    tag_size  = struct.pack('>I', self.payload_size + 11)
    return (pkt_type + data_size + timestamp[0] + timestamp[1] +
            b'\x00\x00\x00' + self.payload + tag_size)

  def __str__(self):
    return ("Pkt Type: %02x, Payload Size: %d, "
            "Timestamp: %d, Stream ID: %d" ) % (
            self.pkt_type, self.payload_size,
            self.timestamp, self.stream_id)

def main():
  with open(sys.argv[1], 'rb') as f:
    with open(sys.argv[2], 'rb') as f2:
      with open(sys.argv[3], 'wb') as o:
        h = hdr()
        h.parse(f.read(h.to_read()))
        if h.to_read() > 0:
          h.handle_header(f.read(h.to_read()))
        print(h)
        o.write(h.write())

        h = hdr()
        h.parse(f2.read(h.to_read()))
        if h.to_read() > 0:
          h.handle_header(f2.read(h.to_read()))
        print(h)

        while True:
          try:
            p = flv_previous_tags_info()
            data = f.read(p.to_read())
            p.parse(data)
            print(p)

            p = flv_tags()
            data = f.read(p.to_read())
            p.parse(data)
            while p.to_read() > 0:
              data = f.read(p.to_read())
              p.handle_payload(data)
            print(p)
            last_ts = p.timestamp
            o.write(p.write())
          except struct.error:
            break
        while True:
          try:
            p = flv_previous_tags_info()
            data = f2.read(p.to_read())
            p.parse(data)
            print(p)

            p = flv_tags()
            data = f2.read(p.to_read())
            p.parse(data)
            while p.to_read() > 0:
              data = f2.read(p.to_read())
              p.handle_payload(data)
            p.add_ts(last_ts)
            print(p)
            o.write(p.write())
          except struct.error:
            break

if __name__ == '__main__':
  main()
